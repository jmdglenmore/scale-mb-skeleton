scale-mb-skeleton
============

Contains a skeleton course that incorporates YAML quiz format 

##Introduction

This is a skeleton course that can be used with the mbscale project, it incorporates a yaml format quiz that is parsed into json and presented to the student as part of a javascript quiz.

##Structure

~~~
├── course.md
├── credits
├── topic01
│   ├── book
│   │   ├── 00.Lab-01.md
│   │   ├── 01.01.md
│   │   ├── 02.02.md
│   │   ├── 03.03.yaml
│   │   ├── 04.04.md
│   │   ├── 05.05.md
│   │   ├── 06.Exercises.md
│   │   ├── archives
│   │   │   └── archive.zip
│   │   └── img
│   │       ├── 01.png
│   │       ├── 02.png
│   │       └── 03.png
│   ├── pdf
│   │   ├── slides-1.pdf
│   │   ├── slides-2.pdf
│   │   └── slides-3.pdf
│   └── topic.md
~~~

##Notes

At the moment only **one** yaml file is permitted per topic but this can have as many questions as you want to include


The questions have two types available

multiple choice -- single answer

or

multiple choice -- multiple answer


###Contents of 03.03.yaml
~~~
questions     :
    - question: "Question 1"
      answers:
          - "A"
          - "B"
          - "C"
          - "D"
      correct_answer: "0" 
    - question: "Question 2"
      answers:
          - "1"
          - "2"
          - "3"
          - "4"
      correct_answer: "3" 
    - question: "Question 3 (Multi Answer)"
      answers:
          - "A1"
          - "B2"
          - "C3"
          - "D4"
      correct_answer: "2,3"
~~~

